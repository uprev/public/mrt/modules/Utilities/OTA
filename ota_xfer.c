/**
  *@file ota_xfer.c
  *@brief header for 
  *@author jason
  *@date 07/24/23
  */

/* Includes ------------------------------------------------------------------*/
#include "ota_xfer.h"

/* Private Variables ---------------------------------------------------------*/

//Mark block as received
void mark_block(ota_xfer_t* xfer, int blockNum)
{

  if((blockNum > -1) && (blockNum < xfer->blockCount))
  {
    xfer->blockMask[(blockNum / 8)] |= (0x80 >> (blockNum % 8));
  }
  
}

int get_block_from_addr(ota_xfer_t* xfer, uint32_t addr)
{ 

  return addr / xfer->img_mgr->blockSize;

}

/* Functions -----------------------------------------------------------------*/

void ota_xfer_init(ota_xfer_t* xfer)
{
  memset(xfer, 0, sizeof(ota_xfer_t));
  xfer->img_mgr = NULL;
  xfer->state = OTA_XFER_STATE_IDLE;
  
}

ota_status_t ota_xfer_start(ota_xfer_t* xfer, ota_img_mgr_t* mgr, const char* label, const char* strVersion, uint32_t size, uint32_t crc)
{
  xfer->size = size;
  xfer->crc = crc;
  xfer->blockCount = (size / mgr->blockSize) + 1;
  xfer->blockMaskLen = (xfer->blockCount / 8) + 1;
  xfer->blockMask = (uint8_t*)malloc(xfer->blockMaskLen);
  memset(xfer->blockMask, 0, xfer->blockMaskLen);
  xfer->lastRequestedAddr = -1;
  xfer->state = OTA_XFER_STATE_STARTED;
  xfer->img_mgr = mgr;
  xfer->retryCount = OTA_XFER_DEFAULT_RETRY_COUNT;


  crc32_init(&xfer->crc_ctx); //Reset CRC32


  //Fill in image/partition info
  xfer->part = ota_img_mgr_get_partition(mgr, label);

  if(xfer->part != NULL)
  {
    ota_partition_clear(xfer->part);
    xfer->part->image.crc = crc;
    xfer->part->image.size = size;
    xfer->part->flags = 0;
    strcpy(xfer->part->image.strVersion, strVersion);

    return OTA_STATUS_OK;
  }

  return OTA_STATUS_ERROR;

}

void ota_xfer_deinit(ota_xfer_t* xfer)
{
  free(xfer->blockMask);
}

void ota_xfer_set_state(ota_xfer_t* xfer, uint8_t state)
{
  xfer->state = state;
}

void ota_xfer_retry(ota_xfer_t* xfer)
{
  xfer->retryCount --; 

  if(xfer->retryCount == 0)
  {
    ota_xfer_set_state(xfer, OTA_XFER_STATE_IDLE);
    memset(xfer->blockMask, 0, xfer->blockMaskLen);
    xfer->lastRequestedAddr = -1;
  }
}

uint32_t ota_xfer_write_block(ota_xfer_t* xfer, uint32_t addr, uint8_t* data, uint32_t len)
{

  uint32_t wLen = len; 

  if((addr >= xfer->size) || (xfer->part == NULL))
  {
    return 0;
  }
  else if((addr + wLen) > xfer->size)
  {
    wLen = xfer->size - addr;
  }


  crc32_update(&xfer->crc_ctx, data, wLen);
  ota_partition_write_image(xfer->part, addr, data, wLen);
  int blockNum = get_block_from_addr(xfer, addr);
  mark_block(xfer, blockNum);

  return wLen;
}


int ota_xfer_get_next_missing_block(ota_xfer_t* xfer)
{

  int nextMissing = OTA_XFER_NO_MISSING_BLOCKS;
  //Find the next missing block
  for(int i=0; i < xfer->blockMaskLen; i++)
  {
    if( xfer->blockMask[i] != 0xFF)
    {
      for(int j=0; j < 8; j++)
      {
        if( (xfer->blockMask[i] & (0x80 >> j)) == 0)
        {
          nextMissing = (i*8) + j;
          if(nextMissing >= xfer->blockCount)
          {
            nextMissing = OTA_XFER_NO_MISSING_BLOCKS;
          }
          return nextMissing;
        }
      }
    }
  }


  return nextMissing;
}

ota_status_t ota_xfer_verify_img(ota_xfer_t* xfer)
{
  uint32_t xferCrc = crc32_result(&xfer->crc_ctx);

  if(xferCrc == xfer->crc)
  {
    ota_partition_set_flag(xfer->part, OTA_PARTITION_FLAG_VERIFIED);
    return OTA_STATUS_OK;
  }

  return OTA_STATUS_ERROR;
}

