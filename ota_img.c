/**
  *@file ota.c
  *@brief header for 
  *@author jason
  *@date 07/20/23
  */

/* Includes ------------------------------------------------------------------*/
#include "ota_img.h"
#include "Utilities/CRC/crc32.h"

/* Private Variables ---------------------------------------------------------*/


/* Functions -----------------------------------------------------------------*/

void ota_img_mgr_init(ota_img_mgr_t* mgr)
{
  mgr->blockSize = OTA_IMG_MGR_DEFAULT_BLOCK_SIZE;
  memset(mgr->dsks,0,sizeof(ota_dsk_t) * OTA_IMG_MGR_MAX_DSKS);
}

ota_dsk_t* ota_img_mgr_add_dsk(ota_img_mgr_t* mgr,const char* label, uint32_t offset , f_ota_dsk_write fWrite, f_ota_dsk_read fRead, f_ota_dsk_erase fErase)
{
    mgr->dskCount++; 
    if(mgr->dskCount > OTA_IMG_MGR_MAX_DSKS)
    {
      return NULL;
    }

    ota_dsk_init(&mgr->dsks[mgr->dskCount - 1], label,  offset, fWrite, fRead, fErase);
    mgr->dsks[mgr->dskCount - 1].mgr = mgr;

  return &mgr->dsks[mgr->dskCount - 1];
}

ota_dsk_t* ota_img_mgr_get_dsk(ota_img_mgr_t* mgr, const char* path)
{
  ota_dsk_t* dsk = NULL;

  char* pathCopy = strdup(path);
  char* dskLabel = strtok(pathCopy, "/");

  //Find the dsk with the given label
  for(int i=0; i < mgr->dskCount; i++)
  {
    if(strcmp(mgr->dsks[i].label, dskLabel) == 0)
    {
      dsk = &mgr->dsks[i];
      break;
    }
  }

  free(pathCopy);

  return dsk;
}

ota_status_t ota_dsk_clear(ota_dsk_t *dsk)
{
  ota_status_t status = OTA_STATUS_OK;
  uint8_t* eraser; 

  
  if(dsk->partitionCount > 0)
  { 

    if(dsk->erase)
    {
      dsk->erase(dsk->partitionTableAddr, OTA_PARTITION_TABLE_ALIGNMENT * dsk->partitionCount);
    }
    else 
    {
      eraser =  malloc(OTA_PARTITION_TABLE_ALIGNMENT);
      memset(eraser,0xFF,OTA_PARTITION_TABLE_ALIGNMENT);
      for(int i=0; i < dsk->partitionCount; i++)
      {
        dsk->write(dsk->partitionTableAddr + (i * OTA_PARTITION_TABLE_ALIGNMENT) , eraser, OTA_PARTITION_TABLE_ALIGNMENT);
      }
      free(eraser);
    
    }
    free(dsk->partitions);
    dsk->partitionCount = 0;
    dsk->partitions = NULL;
    
  }

  return status;
}


ota_partition_t* ota_img_mgr_get_partition(ota_img_mgr_t* mgr, const char* path)
{
  ota_partition_t* part = NULL; 
  //create copy of path 
  char* pathCopy = strdup(path);

  char* dskLabel = strtok(pathCopy, "/");
  char* partLabel = strtok(NULL, "/");

  ota_dsk_t* dsk = ota_img_mgr_get_dsk(mgr, dskLabel);

  if(dsk != NULL)
  {
    part = ota_dsk_get_partition(dsk, partLabel);
  }

  free(pathCopy);

  return part;
}

ota_partition_t* ota_img_mgr_get_partition_by_id(ota_img_mgr_t* mgr, uint32_t dskId, uint32_t partId)
{
  ota_partition_t* part = NULL; 

  ota_dsk_t* dsk = NULL;

  if(dskId < mgr->dskCount)
  {
    dsk = &mgr->dsks[dskId];
  }


  if(dsk != NULL && partId < dsk->partitionCount)
  {
    part = &dsk->partitions[partId];
  }

  return part;
}

ota_partition_t* ota_dsk_get_partition(ota_dsk_t* dsk, const char* label)
{
  ota_partition_t* part = NULL;

  //Find the partition with the given label
  for(int i=0; i < dsk->partitionCount; i++)
  {
    if(strcmp(dsk->partitions[i].label, label) == 0)
    {
      part = &dsk->partitions[i];
      part->dsk = dsk;
      break;
    }
  }

  return part;
}


uint16_t ota_dsk_init(ota_dsk_t *dsk, const char* label, uint32_t offset,  f_ota_dsk_write write, f_ota_dsk_read read,  f_ota_dsk_erase erase)
{   

    ota_status_t status = OTA_STATUS_OK;
    dsk->partitionCount = 0; 
    dsk->partitions = NULL;
    dsk->partitionTableAddr = offset;
    dsk->write = write;
    dsk->read = read;
    dsk->erase = erase;
    dsk->nextPartId = 0;
    strcpy(dsk->label, label);

    //If we have a read function, load in the partitions
    if(dsk->read != NULL)
    {
      

      //Get partition count 
      uint32_t addr = offset;
      ota_partition_t tmpPart; 

      //Do an initial pass through and just count the number of partitions
      do {

        tmpPart.preamble = 0;//Clear preamable so we know flash read is functioning properly when we get one

        //Read in partition header
        status = dsk->read(addr, (uint8_t*)&tmpPart, sizeof(ota_partition_t));

        if(status != OTA_STATUS_OK)
        {
          return status;
        }

        if(tmpPart.preamble == OTA_PARTITION_PREAMBLE)
        {
          dsk->partitionCount++;
          addr += OTA_PARTITION_TABLE_ALIGNMENT;
        }


      } while(tmpPart.preamble == OTA_PARTITION_PREAMBLE);

      //Allocate memory for partition list
      dsk->partitions = (ota_partition_t*)malloc(sizeof(ota_partition_t) * dsk->partitionCount);

      //Do a second pass and read in the partition headers
      addr = offset;

      for(int i=0; i < dsk->partitionCount; i++)
      {
        //Read in partition header
        dsk->read( addr, (uint8_t*)&tmpPart, sizeof(ota_partition_t));

        if(tmpPart.preamble == OTA_PARTITION_PREAMBLE)
        {
          memcpy(&dsk->partitions[i], &tmpPart, sizeof(ota_partition_t));
          dsk->partitions[i].id = dsk->nextPartId++;
          addr += OTA_PARTITION_TABLE_ALIGNMENT;
        }
      }
    }


    return OTA_STATUS_OK;
}

void ota_dsk_deinit(ota_dsk_t *dsk)
{
  if(dsk->partitions != NULL)
  {
    free(dsk->partitions);
  }
  if(dsk->label != NULL)
  {
    free(dsk->label); 
  }
}

ota_status_t ota_dsk_add_partition(ota_dsk_t *dsk, uint32_t addr, uint32_t size, const char *label)
{


  ota_partition_t newPart;      //Partition being added

  if(addr == OTA_PARTITION_ADDR_AUTO )
  {

    if(dsk->partitionCount == 0)
    {
      //If this is being used for the first partition, just assume a max of 8 partitions will be used.
      addr = dsk->partitionTableAddr + (8 * OTA_PARTITION_TABLE_ALIGNMENT);
    }
    else
    {
      
      //If we are auto assigning the address, then we need to find the last partition and add the size to it
      addr = dsk->partitions[dsk->partitionCount-1].addr + dsk->partitions[dsk->partitionCount-1].size;
    }

  }

  newPart.preamble = OTA_PARTITION_PREAMBLE;
  newPart.addr = addr;
  newPart.size = size;
  newPart.flags = OTA_PARTITION_FLAG_NONE;
  newPart.id = dsk->nextPartId++;
  memcpy(newPart.label, label, OTA_PARTITION_LABEL_LEN);
  memset(&newPart.image, 0, sizeof(ota_image_t));
  
  //Create new re-sized partition
  ota_partition_t* newList = (ota_partition_t*)malloc(sizeof(ota_partition_t) * (dsk->partitionCount + 1));
  memcpy(newList, dsk->partitions, sizeof(ota_partition_t) * dsk->partitionCount);
  memcpy(&newList[dsk->partitionCount], &newPart, sizeof(ota_partition_t));
  free(dsk->partitions);
  dsk->partitions = newList;
  dsk->partitionCount++;


  return OTA_STATUS_OK;      
}


ota_status_t ota_dsk_commit_partition_table(ota_dsk_t *dsk)
{
  //Write the partition table
  ota_status_t status = OTA_STATUS_OK;

  //Create a tmp buffer to keep proper alignment of records
  uint8_t* tmp = (uint8_t*)malloc(OTA_PARTITION_TABLE_ALIGNMENT * dsk->partitionCount);
  memset(tmp, 0, OTA_PARTITION_TABLE_ALIGNMENT * dsk->partitionCount);

  for(int i=0; i < dsk->partitionCount; i++)
  {
    memcpy(tmp + (i * OTA_PARTITION_TABLE_ALIGNMENT), (void*)&dsk->partitions[i], sizeof(ota_partition_t));
  }

  if(dsk->erase != NULL)
  {
    status = dsk->erase(dsk->partitionTableAddr, OTA_PARTITION_TABLE_ALIGNMENT * dsk->partitionCount);
  }

  //write the partition table
  status = dsk->write( dsk->partitionTableAddr, tmp, OTA_PARTITION_TABLE_ALIGNMENT * dsk->partitionCount);

  return status;
}


ota_status_t ota_partition_verify_image(ota_partition_t* part,  f_ota_progress_cb progress_cb )
{
  ota_status_t status = OTA_STATUS_OK;

  //Verify the image using the crc.
  crc32_ctx_t crc;
  crc32_init(&crc);

  uint8_t* block = (uint8_t*)malloc(part->dsk->mgr->blockSize);
  uint32_t blockSize = part->dsk->mgr->blockSize;
  uint32_t len;

  for(uint32_t i=0; i < part->image.size; i += len)
  {
    len = blockSize;


    if(len > part->image.size - i)
    {
      len = part->image.size - i;
    }

    part->dsk->read( part->addr + i, block, len);

    if(progress_cb != NULL)
    {
      progress_cb(i,part->image.size);
    }

    crc32_update(&crc, block, len);
  }

  uint32_t result = crc32_result(&crc);
  free(block);

  if(result != part->image.crc)
  {
    return OTA_STATUS_ERROR;
  }

  part->flags |= OTA_PARTITION_FLAG_VERIFIED;

  return OTA_STATUS_OK;

}

uint32_t ota_partition_write_image( ota_partition_t* part, uint32_t offset, uint8_t *data, uint32_t len)
{

  uint32_t wLen = len;
  
  if (offset >= part->size || part == NULL)
  {
    return 0;  // write goes out of bounds
  }
  else if(offset + len > part->size)
  {
    wLen = part->size - offset;
  }


  //If we found the partition, write the data
  if(part != NULL)
  {
    part->dsk->write(part->addr + offset, data, wLen);
  }

  return wLen;
}


uint32_t ota_partition_read_image( ota_partition_t* part,uint32_t offset, uint8_t *data, uint32_t len)
{

  ota_status_t status; 

  if( offset + len > part->size)
  {
    len = part->size - offset;
  }


  //If we found the partition, read the data
  if(part != NULL)
  {
    if(part->dsk->read( part->addr + offset, data, len) == OTA_STATUS_OK)
    {
      return len;
    }

  }

  return 0;
}

ota_status_t ota_partition_clear(ota_partition_t* part)
{

  //Clear the partition image header
  part->image.crc = 0;
  part->image.size = 0;
  part->image.strVersion[0] = 0;
  part->flags = 0;

  //Clear data 
  if(part->dsk->erase != NULL)
  {
    part->dsk->erase(part->addr, part->size);
  }


  return ota_dsk_commit_partition_table(part->dsk);

}

ota_status_t ota_partition_copy(ota_partition_t* src, ota_partition_t* dst,  f_ota_progress_cb progress_cb )
{
  ota_status_t status = OTA_STATUS_OK;
  if((src->dsk->write == NULL) || (src->dsk->read == NULL) || (src->dsk->write == NULL) || (src->dsk->read == NULL))
  {
    return OTA_STATUS_ERROR; 
  }

  uint32_t blockSize = src->dsk->mgr->blockSize; 
  uint8_t* block = (uint8_t*)malloc(blockSize);
  uint32_t len; 


  //Copy Image header
  dst->image.size = src->image.size;
  dst->image.crc = src->image.crc;
  dst->flags = OTA_PARTITION_FLAG_NONE; //Clear flags on destination partition
  strcpy(dst->image.strVersion, src->image.strVersion);
  
  //Copy image data
  for(uint32_t i=0; i < src->image.size; i += len)
  {
    len = blockSize;

    if(len > src->image.size - i)
    {
      len = src->image.size - i;
    }

    src->dsk->read(src->addr + i, block, len);
  
    dst->dsk->write(dst->addr + i, block, len);

    if(progress_cb != NULL)
    {
      progress_cb(i,src->image.size);
    }

  }


  status = ota_partition_verify_image(dst, progress_cb);
  ota_dsk_commit_partition_table(dst->dsk);

  free(block);
  return status;
}


ota_status_t ota_partition_copy_to_dsk(ota_partition_t* src, ota_dsk_t* dstDsk, uint32_t dstAddr,  f_ota_progress_cb progress_cb )
{
    ota_status_t status = OTA_STATUS_OK;
    if((src->dsk->write == NULL) || (src->dsk->read == NULL) || (src->dsk->write == NULL) || (src->dsk->read == NULL))
    {
      return OTA_STATUS_ERROR; 
    }

    uint32_t blockSize = src->dsk->mgr->blockSize; 
    uint8_t* block = (uint8_t*)malloc(blockSize);
    uint32_t len; 
    
    //Copy image data
    for(uint32_t i=0; i < src->image.size; i += len)
    {
      len = blockSize;

      if(len > src->image.size - i)
      {
        len = src->image.size - i;
      }

      src->dsk->read(src->addr + i, block, len);
    
      dstDsk->write(dstAddr + i, block, len);

      if(progress_cb != NULL)
      {
        progress_cb(i,src->image.size);
      }
      
    }

    //Verify Image 

    
  //Verify the image using the crc.
  crc32_ctx_t crc;
  crc32_init(&crc);

  for(uint32_t i=0; i < src->image.size; i += len)
  {
    len = blockSize;

    if(len > src->image.size - i)
    {
      len = src->image.size - i;
    }

    dstDsk->read(dstAddr+ i, block, len);

    crc32_update(&crc, block, len);
  }

  uint32_t result = crc32_result(&crc);
  free(block);

  if(result != src->image.crc)
  {
    return OTA_STATUS_ERROR;
  }


  return OTA_STATUS_OK;
}

ota_status_t ota_partition_clear_flag(ota_partition_t* part, uint32_t flag)
{
  part->flags &= ~flag;
  return ota_dsk_commit_partition_table(part->dsk);
}
ota_status_t ota_partition_set_flag(ota_partition_t* part, uint32_t flag)
{
  part->flags |= flag;
  return ota_dsk_commit_partition_table(part->dsk);
}

