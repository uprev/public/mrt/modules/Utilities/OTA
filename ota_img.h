/**
  *@file ota.h
  *@brief header for 
  *@author jason
  *@date 07/20/23
  */

#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include <string.h>





/* Exported macro ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

#define OTA_STATUS_OK 0
#define OTA_STATUS_ERROR 1


#define OTA_PARTITION_TABLE_ALIGNMENT  256 // Partition table entries must be aligned to 256 bytes

#define OTA_IMG_MGR_DEFAULT_BLOCK_SIZE 1024
#define OTA_IMG_MGR_MAX_DSKS  8
#define OTA_PARTITION_LABEL_LEN 16        // Max length of partition label

#define OTA_IMAGE_STR_VERSION_LEN 16      // Max length of image version string

#define OTA_PARTITION_FLAG_NONE           0x00000000  
#define OTA_PARTITION_FLAG_ACTIVE         0x00000001  // Indicates an active partition. Used for ping-pong style booting
#define OTA_PARTITION_FLAG_VERIFIED       0x00000002  // Indicates the image has been verified with the CRC
#define OTA_PARTITION_FLAG_NEW            0x00000004  // Indicates this is a new update image which has not been applied
#define OTA_PARTITION_FLAG_SELF_TEST      0x00000008  // Indicates the image in this partition passed the self-test


#define OTA_PARTITION_PREAMBLE     0xABCDABCD  //Inidicates this is a header record

#define OTA_PARTITION_ADDR_AUTO    0xFFFFFFFF  //Indicates the partition should be placed immediately after the previous partition



/* Exported types ------------------------------------------------------------*/

typedef uint32_t ota_status_t;

struct ota_dsk_s;


typedef ota_status_t (*f_ota_dsk_write)( uint32_t addr, uint8_t *data, uint32_t len);
typedef ota_status_t (*f_ota_dsk_read)(uint32_t addr, uint8_t *data, uint32_t len);
typedef ota_status_t (*f_ota_dsk_erase)(uint32_t addr, uint32_t len);
typedef void (*f_ota_progress_cb)(uint32_t progress, uint32_t total);

struct ota_img_mgr_s;

typedef struct{
  uint32_t size;                                // size of the image in bytes
  uint32_t crc;                                 // CRC32 of the image
  char strVersion[OTA_IMAGE_STR_VERSION_LEN];   // String version of the image
}ota_image_t;


// OTA partition
typedef struct 
{
  uint32_t preamble;                     // partition preamble 0xABCDABCD when valid
  uint16_t id;                           // partition id 
  uint32_t addr;                         // Address of partition
  uint32_t size;                         // size of the partition in bytes
  uint32_t flags;                        // Flags for the Partition/Image
  char label[OTA_PARTITION_LABEL_LEN];   // label of the section
  ota_image_t image;                     // Image info
  struct ota_dsk_s* dsk;                 // Pointer to the dsk this partition is on
}ota_partition_t;


/**
 * @brief Represents a storage medium containing OTA records/images 
 * This could be a flash memory, EEPROM, SD card etc
 */
typedef struct ota_dsk_s {
  uint16_t partitionCount;            // Number of records in the medium
  ota_partition_t *partitions;        // Array of partitions
  uint32_t partitionTableAddr;        // Address of the first record in partition table
  f_ota_dsk_write write;              // Function to write data to the staging medium
  f_ota_dsk_read read;                // Function to read data from the staging medium
  f_ota_dsk_erase erase;              // Function to erase data from the staging medium (really only needed for Flash that requires erase cycles)
  struct ota_img_mgr_s* mgr;          // Pointer to the OTA context
  char label[OTA_PARTITION_LABEL_LEN];   // label of the section
  uint16_t nextPartId;
}ota_dsk_t;


typedef struct ota_img_mgr_s {
  uint32_t blockSize;                         //blockSize for transfers
  uint32_t dskCount;                          //Number of dsks
  ota_dsk_t dsks[OTA_IMG_MGR_MAX_DSKS];       //Array of dsks
}ota_img_mgr_t;

/* Exported functions ------------------------------------------------------- */

/**
 * @brief Initialize the OTA image manager
 * 
 * @param mgr 
 */
void ota_img_mgr_init(ota_img_mgr_t* mgr);

/** 
 * @brief Add a new dsk to the OTA image manager
 * @param mgr ptr to OTA image manager
 * @param label string label of dsk
 * @param offset offset of the partition table
 * @param fWrite function pointer to write to  dsk
 * @param fRead function ptr to read from  dsk
 * @param fErase function ptr to erase from dsk
 * @return ptr to dsk struct
*/
ota_dsk_t* ota_img_mgr_add_dsk(ota_img_mgr_t* mgr,const char* label, uint32_t offset, f_ota_dsk_write fWrite, f_ota_dsk_read fRead, f_ota_dsk_erase fErase);

/**
 * @brief Get a dsk from the OTA image manager by label
 * @param mgr ptr to OTA image manager
 * @param path string path of dsk or partition
 * @return ptr to dsk struct
*/
ota_dsk_t* ota_img_mgr_get_dsk(ota_img_mgr_t* mgr, const char* path);

/**
 * @brief Get a partition from the OTA image manager by path (dsk label / partition label)
 * 
 * @param mgr ptr to image manager
 * @param path dsk label / partition label
 * @return ota_partition_t* 
 */
ota_partition_t* ota_img_mgr_get_partition(ota_img_mgr_t* mgr,  const char* path);


/**
 * @brief Gets partition by dsk and partition id
 * 
 * @param mgr 
 * @param dskId 
 * @param partId 
 * @return ota_partition_t 
 */
ota_partition_t* ota_img_mgr_get_partition_by_id(ota_img_mgr_t* mgr, uint32_t dskId, uint32_t partId);

/**
 * @brief Initialize the OTA dsk. This will read the records from the dsk and populate the records array.
 * @param dsk ptr to dsk struct
 * @param offset offset of the first partition
 * @param write function pointer to write to  dsk 
 * @param read function ptr to read from  dsk
 * @param erase function ptr to erase from dsk
 * @return number of records found or 0 if no records found
 */
uint16_t ota_dsk_init(ota_dsk_t *dsk, const char* label, uint32_t ptAddr, f_ota_dsk_write write, f_ota_dsk_read read, f_ota_dsk_erase erase);

/**
 * @brief Deinitialize the OTA dsk. This will free the records array.
 * @param dsk ptr to dsk struct
 */
void ota_dsk_deinit(ota_dsk_t *dsk);

/**
 * @brief Add a new partition to the dsk
 * @param dsk ptr to dsk struct
 * @param addr address for the partition
 * @param size size of the partition
 * @param label string label of partition
 * @return status 
 */
ota_status_t ota_dsk_add_partition(ota_dsk_t *dsk, uint32_t addr, uint32_t size, const char *label);

/**
 * @brief Commits the partition table to the dsk, writing any changes to memory
 * @param dsk 
 * @return ota_status_t 
 */
ota_status_t ota_dsk_commit_partition_table(ota_dsk_t *dsk);

/**
 * @brief Clear the dsk removing all partitions
 * @param dsk 
 * @return ota_status_t 
 */
ota_status_t ota_dsk_clear(ota_dsk_t *dsk);


/**
 * @brief Get a partition from the dsk by label 
 * @param dsk ptr to dsk struct
 * @param label string label of partition
 * @return ptr to partition or NULL if not found
 */
ota_partition_t* ota_dsk_get_partition(ota_dsk_t* dsk, const char* label);



/**
 * @brief Set the active partition (will unset previously active partition)
 * @param part ptr to partition struct
 * @return status
*/
ota_status_t ota_partition_set_active(ota_partition_t* part);


/**
 * @brief verify image CRC
 * @param part ptr to partition struct
 * @param progress_cb progress callback function for progress updates
 * @return status
*/
ota_status_t ota_partition_verify_image(ota_partition_t* part, f_ota_progress_cb progress_cb );


/**
 * @brief write image data to the partition
 * @param part ptr to partition struct
 * @param offset offset to write to
 * @param data ptr to data to write
 * @param len length of data to write
 * @return number of bytes written
*/
uint32_t ota_partition_write_image( ota_partition_t* part, uint32_t offset, uint8_t *data, uint32_t len);

/**
 * @brief read image data from the partition
 * @param part ptr to partition struct
 * @param offset offset to read from
 * @param data ptr to data to read
 * @param len length of data to read
 * @return number of bytes read
*/
uint32_t ota_partition_read_image( ota_partition_t* part, uint32_t offset, uint8_t *data, uint32_t len);


/**
 * @brief Clear partition removing image
 * @param part - ptr to partition
 * @return ota_status_t 
 */
ota_status_t ota_partition_clear(ota_partition_t* part);

/**
 * @brief copy partition data from src to dst
 * @param src ptr to source partitiong
 * @param dst ptr to destination partition
 * @param progress_cb progress callback function for progress
 * @return ota_status_t 
 */
ota_status_t ota_partition_copy(ota_partition_t* src, ota_partition_t* dst, f_ota_progress_cb progress_cb );

/**
 * @brief apply the image to the destination dsk. Used when the destination dsk does not use partitions
 * @param part ptr to partition struct
 * @param dstDsk ptr to destination dsk
 * @param dstAddr address to apply image to
 * @param progress_cb progress callback function for progress
 * @return ota_status_t 
 */
ota_status_t ota_partition_copy_to_dsk(ota_partition_t* src, ota_dsk_t* dstDsk, uint32_t dstAddr,  f_ota_progress_cb progress_cb );

/**
 * @brief Clears flag on partition and commits changes
 * 
 * @param part partition
 * @param flag flag(s) to clear
 * @return ota_status_t 
 */
ota_status_t ota_partition_clear_flag(ota_partition_t* part, uint32_t flag);

/**
 * @brief Sets flag on partition and commits changes
 * 
 * @param part partition
 * @param flag flag(s) to set
 * @return ota_status_t 
 */
ota_status_t ota_partition_set_flag(ota_partition_t* part, uint32_t flag);




#ifdef __cplusplus
}
#endif



