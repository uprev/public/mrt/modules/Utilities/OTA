OTA Poly
========

This folder contains a protocol descriptor file for generating an OTA protocol using `PolyPacket <https://mrt.readthedocs.io/en/latest/pages/polypacket/polypacket.html>`_ and a utility script for interacting with the protocol.


Using the Protocol 
------------------

The protocol file can be used to generate a standalone protocol or included in an existing protocol using the 'plugins' directive. 





